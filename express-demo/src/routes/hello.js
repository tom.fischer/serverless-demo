const express = require("express");
const HttpStatus = require("http-status-codes");

const router = express.Router({ mergeParams: true });

const hello = (req, res) =>
  res.status(HttpStatus.OK).json({ message: "Hello, World!" });

router.get("/", hello);

module.exports = router;
