import React from "react";
import { RequestProvider } from "react-request-hook";
import axios from "axios";

import Hello from "../Hello";
import "./App.css";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL
});

const App = () => (
  <RequestProvider value={axiosInstance}>
    <Hello />
  </RequestProvider>
);

export default App;
