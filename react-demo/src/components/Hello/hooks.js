import { useEffect } from "react";
import { useResource } from "react-request-hook";

export default () => {
  const [{ data, isLoading }, fetchMessage] = useResource(() => ({
    url: "/api/hello",
    method: "GET"
  }));

  useEffect(() => {
    fetchMessage();
  }, [fetchMessage]);

  return { data, isLoading };
};
