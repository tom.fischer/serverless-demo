import React, { Fragment } from "react";

import logo from "./logo.svg";
import useHello from "./hooks";
import "./Hello.css";

const Hello = () => {
  const { data, isLoading } = useHello();
  return (
    <div className="Hello">
      <header className="Hello-header">
        <img src={logo} className="Hello-logo" alt="logo" />
        {!isLoading && (
          <Fragment>
            <p>{data && data.message}</p>
            <a
              className="Hello-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
          </Fragment>
        )}
      </header>
    </div>
  );
};
export default Hello;
